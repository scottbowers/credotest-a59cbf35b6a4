﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TasksApplication.Models;

namespace TasksApplication.Controllers
{
    public class TasksController : ApiController
    {
        //sample data, in real app would be getting data from the database.
        Task[] tasks = new Task[]
        {};

        public object DBContext { get; private set; }

        public IEnumerable<Task> GetAllTasks()
        {
            return tasks;
        }

        public IHttpActionResult GetTask(int id)
        {          
            var task = tasks.FirstOrDefault((p) => p.Id == id);
            if (task == null)
            {
                return NotFound();
            }
            return Ok(task);
        }

        public Task PostTask(Task value)
        {
            // this would add the task to the database here
            //var context = new DatabaseEntities();
            //context.Task.Add(value);
            //context.SaveChanges();
            return value;
        }

        public IHttpActionResult Put(Task value)
        {
            // this would update the task in the database
            var task = tasks.FirstOrDefault((p) => p.Id == value.Id);
            if (task == null)
            {
                return NotFound();
            }
            else
            {
                task.Completed = !value.Completed;
                //context.SaveChanges();
            }
            return Ok(task);
        }
    }
}
